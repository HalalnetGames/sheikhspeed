const config = {
	mode: 'jit',
	purge: ['./src/**/*.{html,js,svelte,ts}'],
	theme: {
		fontFamily: {
			'ub-mono': 'Ubuntu Mono',
			ub: 'Ubuntu',
			'djvs-mono': 'dejavu-sans-mono',
			rubik: 'rubik',
			'fira-sans': 'fira-sans',
			'ss-pro': 'source-sans-pro',
			'iosevka-aile': 'iosevka-aile',
			chivo: 'Chivo',
			krub: 'Krub',
			'enc-sans': 'Encode Sans',
			'jb-mono': 'JetBrains Mono',
			'open-sans': 'Open Sans',
			pdms: 'pdms',
			hafs: 'hafs',
			bsml: 'bsml',
			'fa-solid': 'fa-solid'
		},
		extend: {
			screens: {
				rs: { raw: '(max-aspect-ratio: 1/1)' }
			},
			colors: {
				'onedark-black': '#282c34',
				'onedark-white': '#abb2bf',
				'onedark-purple': '#c678dd',
				'onedark-red': '#e06c75',
				'onedark-blue': '#61afef',
				'onedark-cyan': '#56b6c2',
				'onedark-green': '#98c379',
				'onedark-yellow': '#e5c07b'
			}
		}
	},
	plugins: [require('tailwind-nord'), require('tailwind-dracula')('dracula')]
};

module.exports = config;
